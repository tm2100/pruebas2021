/*Escriba un programa para solicitar el nombre de una persona
 y luego mostrar el mensaje �Hola �,nombre,� mucho gusto en saludarte�. 
Tanto la entrada como la salida de informaci�n debe hacerse por medio de consola. 
Use las clases System y Scanner 
 */
import java.util.Scanner;
public class LeerConsola{
  public static void main(String arg[]){
    String nombre;
    Scanner sc= new Scanner(System.in);
    System.out.println("Digite su nombre");
    nombre=sc.nextLine();
    System.out.println("Hola "+nombre+" mucho gusto en saludarte");
  }//Fin del main

}//Fin de la clase